#include "io_bmp.h"
#include <stdlib.h>

#define BMP_SIGNATURE 0x4D42
#define BMP_HEADER_SIZE 40
#define BYTES_PER_PIXEL 3

// Макрос для размера структуры пикселя
#define PIXEL_SIZE sizeof(struct pixel)


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


static enum read_status read_bmp_header(FILE* in, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header->bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    return READ_OK;
}


static enum write_status write_bmp_header(FILE* out, const struct image* img, struct bmp_header* header) {
    header->bfType = BMP_SIGNATURE;
    header->bfileSize = sizeof(struct bmp_header) + img->width * img->height * BYTES_PER_PIXEL;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = img->width * img->height * BYTES_PER_PIXEL;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;

    if (fwrite(header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}


// Вычисляет выровненный размер строки
static size_t calculate_aligned_row_size(size_t width) {
    return ((width * BYTES_PER_PIXEL) + 3) & ~3;
}


static enum read_status read_bmp_pixels(FILE* in, struct image* img, const struct bmp_header* header) {
    // Выделяем память под массив пикселей
    img->data = (struct pixel*)malloc(img->width * img->height * PIXEL_SIZE);
    if (img->data == NULL) {
        return READ_MEMORY_ERROR;
    }

    // Считываем пиксели изображения
    if (fseek(in, header->bOffBits, SEEK_SET) != 0) {
        free(img->data);
        return READ_INVALID_BITS;
    }

    // Рассчитываем размер строки с учетом выравнивания
    size_t row_size = calculate_aligned_row_size(img->width);

    // Считываем пиксели изображения с учетом выравнивания
    for (uint64_t i = 0; i < img->height; ++i) {
        if (fread(img->data + i * img->width, PIXEL_SIZE, img->width, in) != img->width) {
            free(img->data);
            return READ_INVALID_BITS;
        }
        // Пропускаем выравнивание, если оно есть
        if (fseek(in, (long)(row_size - img->width * PIXEL_SIZE), SEEK_CUR) != 0) {
            free(img->data);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

static enum write_status write_bmp_pixels(FILE* out, const struct image* img) {
    size_t row_size = calculate_aligned_row_size(img->width);

    for (uint64_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, PIXEL_SIZE, img->width, out) != img->width) {
            return WRITE_ERROR;
        }

        for (size_t j = 0; j < row_size - img->width * BYTES_PER_PIXEL; ++j) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}


enum read_status convert_bmp_to_image(FILE* in, struct image* img) {
    struct bmp_header header;
    enum read_status header_status = read_bmp_header(in, &header);

    if (header_status != READ_OK) {
        return header_status;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    enum read_status read_status = read_bmp_pixels(in, img, &header);

    return read_status;
}

enum write_status convert_image_to_bmp(FILE* out, const struct image* img) {
    struct bmp_header header;
    enum write_status header_status = write_bmp_header(out, img, &header);

    if (header_status != WRITE_OK) {
        return header_status;
    }

    enum write_status write_status = write_bmp_pixels(out, img);

    return write_status;
}
