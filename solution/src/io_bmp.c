#include "io_bmp.h"
#include "converter.h"
#include <stdlib.h>


// Чтение BMP файла и конвертация в структуру image
enum read_status read_bmp(const char* const filename, struct image* const img) {
    FILE* const in = fopen(filename, "rb");

    if (in == NULL) {
        return READ_INVALID_HEADER;
    }

    enum read_status const read_status = convert_bmp_to_image(in, img);

    fclose(in);

    return read_status;
}


// Запись структуры image в BMP файл
enum write_status write_bmp(const char* const filename, const struct image* const img) {
    FILE* const out = fopen(filename, "wb");
    if (out == NULL) {
        return WRITE_ERROR;
    }

    enum write_status const write_status = convert_image_to_bmp(out, img);

    fclose(out);

    return write_status;
}
