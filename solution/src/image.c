#include "image.h"
#include <stdlib.h>

void free_image(struct image* img) {
    free(img->data);
    img->width = 0;
    img->height = 0;
    img->data = NULL;
}
