#include "rotator.h"
#include <stdlib.h>

static void swap_dimensions(uint64_t* const width, uint64_t* const height) {
    uint64_t temp = *width;
    *width = *height;
    *height = temp;
}

static struct pixel* allocate_rotated_data(const uint64_t width, const uint64_t height) {
    return (struct pixel*)malloc(width * height * sizeof(struct pixel));
}

static void copy_rotated_pixel(
        const struct pixel* const source_pixel, struct pixel* const dest_pixel,
        const uint64_t new_width, const uint64_t new_i, const uint64_t new_j) {

    dest_pixel[new_i + new_j * new_width] = *source_pixel;
}


static enum rotate_status rotate_90_degrees(struct image* const img) {
    const uint64_t new_width = img->height;
    const uint64_t new_height = img->width;
    struct pixel* const new_data = allocate_rotated_data(new_width, new_height);

    if (new_data == NULL) {
        return ROTATE_MEMORY_ERROR;
    }

    for (uint64_t i = 0; i < img->width; i++) {
        for (uint64_t j = 0; j < img->height; j++) {
            copy_rotated_pixel(&img->data[i + j * img->width], new_data, new_width, j, img->width - 1 - i);
        }
    }

    free(img->data);
    img->data = new_data;
    swap_dimensions(&img->width, &img->height);

    return ROTATE_OK;
}


static enum rotate_status rotate_180_degrees(struct image* const img) {
    const uint64_t new_width = img->width;
    const uint64_t new_height = img->height;
    struct pixel* const new_data = allocate_rotated_data(new_width, new_height);

    if (new_data == NULL) {
        return ROTATE_MEMORY_ERROR;
    }

    for (uint64_t i = 0; i < img->width; i++) {
        for (uint64_t j = 0; j < img->height; j++) {
            copy_rotated_pixel(&img->data[i + j * img->width], new_data, new_width, img->width - 1 - i, img->height - 1 - j);
        }
    }

    free(img->data);
    img->data = new_data;

    return ROTATE_OK;
}


static enum rotate_status rotate_270_degrees(struct image* const img) {
    const uint64_t new_width = img->height;
    const uint64_t new_height = img->width;
    struct pixel* const new_data = allocate_rotated_data(new_width, new_height);

    if (new_data == NULL) {
        return ROTATE_MEMORY_ERROR;
    }

    for (uint64_t i = 0; i < img->width; i++) {
        for (uint64_t j = 0; j < img->height; j++) {
            copy_rotated_pixel(&img->data[i + j * img->width], new_data, new_width, img->height - 1 - j, i);
        }
    }

    free(img->data);
    img->data = new_data;
    swap_dimensions(&img->width, &img->height);

    return ROTATE_OK;
}


enum rotate_status rotate_image(struct image* const img, const enum rotate_angle angle) {
    if (img == NULL || img->data == NULL) {
        return ROTATE_INVALID_IMAGE;
    }

    switch (angle) {
        case ROTATE_ANGLE_0:
            return ROTATE_OK;
        case ROTATE_ANGLE_90:
            return rotate_90_degrees(img);
        case ROTATE_ANGLE_180:
            return rotate_180_degrees(img);
        case ROTATE_ANGLE_270:
            return rotate_270_degrees(img);
        default:
            return ROTATE_INVALID_ANGLE;
    }
}
