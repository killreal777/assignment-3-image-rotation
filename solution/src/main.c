#include "rotator.h"
#include "io_bmp.h"
#include <stdio.h>
#include <stdlib.h>

#define USAGE_MESSAGE "Usage: %s <source-image> <transformed-image> <angle>\n"
#define TRANSFORMATION_SUCCESS "Image transformation completed successfully"
#define ERROR_INVALID_ANGLE "Invalid angle value"
#define ERROR_READING_IMAGE "Error reading image"
#define ERROR_ROTATING_IMAGE "Error rotating image"
#define ERROR_WRITING_IMAGE "Error writing transformed image"

#define ARG_PROGRAM_NAME argv[0]
#define ARG_SOURCE_IMAGE argv[1]
#define ARG_TRANSFORMED_IMAGE argv[2]
#define ARG_ANGLE argv[3]


// Получение угла поворота из строки
enum rotate_angle get_rotate_angle(char *angle_str) {
    char *endptr;

    // Используем функцию strtol для преобразования строки в целочисленное значение
    int64_t angle_value = strtol(angle_str, &endptr, 10);

    // Проверка на ошибки преобразования
    if (*endptr != '\0') {
        return ROTATE_ANGLE_INVALID;
    }

    // Проверка на кратность 90
    if (angle_value % 90 != 0) {
        return ROTATE_ANGLE_INVALID;
    }

    // Приводим значение к перечислению rotate_angle
    return (enum rotate_angle)((angle_value + 360) % 360);
}


// Вывод сообщения об ошибке
void print_error_message(const char *error_message) {
    fprintf(stderr, "Error %s\n", error_message);
}


int main(const int argc, char *const argv[]) {
    // Проверка наличия правильного числа аргументов
    if (argc != 4) {
        fprintf(stderr, USAGE_MESSAGE, ARG_PROGRAM_NAME);
        return 1;
    }

    // Получаем угол поворота из аргументов командной строки
    const enum rotate_angle angle = get_rotate_angle(ARG_ANGLE);

    if (angle == ROTATE_ANGLE_INVALID) {
        print_error_message(ERROR_INVALID_ANGLE);
        return 1;
    }

    // Инициализируем структуру изображения
    struct image img;
    const enum read_status read_status = read_bmp(ARG_SOURCE_IMAGE, &img);

    if (read_status != READ_OK) {
        print_error_message(ERROR_READING_IMAGE);
        free_image(&img);
        return 1;
    }

    // Поворачиваем изображение
    if (rotate_image(&img, angle) != ROTATE_OK) {
        print_error_message(ERROR_ROTATING_IMAGE);
        free_image(&img);
        return 1;
    }

    // Записываем измененное изображение
    const enum write_status write_status = write_bmp(ARG_TRANSFORMED_IMAGE, &img);

    if (write_status != WRITE_OK) {
        print_error_message(ERROR_WRITING_IMAGE);
        free_image(&img);
        return 1;
    }

    // Освобождаем память, выделенную для изображения
    free_image(&img);

    // Выводим сообщение об успешном завершении
    printf("%s\n", TRANSFORMATION_SUCCESS);

    return 0;
}
