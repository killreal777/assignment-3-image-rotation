#ifndef ROTATOR_H
#define ROTATOR_H

#include "image.h"

enum rotate_status {
    ROTATE_OK,
    ROTATE_INVALID_IMAGE,
    ROTATE_INVALID_ANGLE,
    ROTATE_MEMORY_ERROR
};


enum rotate_angle {
    ROTATE_ANGLE_0 = 0,
    ROTATE_ANGLE_90 = 90,
    ROTATE_ANGLE_180 = 180,
    ROTATE_ANGLE_270 = 270,
    ROTATE_ANGLE_INVALID = -1
};

enum rotate_status rotate_image(struct image* img, enum rotate_angle angle);

#endif

