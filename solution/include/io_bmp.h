#ifndef IO_BMP_H
#define IO_BMP_H

#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MEMORY_ERROR
};

enum write_status {
    WRITE_OK,
    WRITE_ERROR
};

enum read_status read_bmp(const char* filename, struct image* img);
enum write_status write_bmp(const char* filename, const struct image* img);

#endif
