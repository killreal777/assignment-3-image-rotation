#ifndef IMAGE_CONVERTER_H
#define IMAGE_CONVERTER_H

#include "image.h"
#include <stdio.h>

enum read_status convert_bmp_to_image(FILE* in, struct image* img);
enum write_status convert_image_to_bmp(FILE* out, const struct image* img);

#endif
